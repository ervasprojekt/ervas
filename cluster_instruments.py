import numpy as np
import sample_file_features
from sklearn.cluster import KMeans

"""
This module applies the k-means algorithm onto the data.
Tested with k = 10.
"""

RANDOM_STATE = 1

y, x = sample_file_features.get_feature_array()

kmeans = KMeans(n_clusters=10, random_state=RANDOM_STATE).fit(x)

print(kmeans.labels_)
print(kmeans.cluster_centers_)

cluster_match = kmeans.predict(x)

for i in range(len(y)):
    print(y[i], cluster_match[i])