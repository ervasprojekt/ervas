import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import sample_file_features


random_state = 1
y, x = sample_file_features.get_feature_array()
classifier = RandomForestClassifier(criterion='entropy', max_depth=6, n_estimators=100, max_features='auto',
                                    random_state=random_state)
x_train, x_test, y_train, y_test = train_test_split(x, y,
                                                    test_size=1/3,
                                                    random_state=random_state,
                                                    stratify=y)
scaler = StandardScaler()
x_train = scaler.fit_transform(x_train)
classifier.fit(x_train, y_train)
score_train = classifier.score(x_train, y_train)

x_test = scaler.transform(x_test)
score_test = classifier.score(x_test, y_test)

y_pred = classifier.predict(x_test)
labels = np.unique(y)
print(labels)
print(confusion_matrix(y_test, y_pred, labels=labels))


print("Random-Forest: Train: {}, Test: {}".format(score_train, score_test))
