import time
import os
import csv
import audioclip
import musicscale
from typing import Tuple, List
import project_settings
from recording import Recording
from sample import Sample


class SoundRecorder:
    """
    Handles the recording of all specified notes for one instrument
    Saves all recordings in a folder and adds an entry to the main .csv file
    """
    WAIT_TIME_BETWEEN_RECORDINGS = 1.0  # in seconds
    NUMBER_OF_REPETITIONS_IN_COUNTDOWN = 3
    TIME_BETWEEN_REPETITIONS_IN_COUNTDOWN = 0.3  # in seconds
    BUFFER_TIME_AFTER_COUNTDOWN = 0.3
    # in seconds. carefully picked value. you should probably not change this
    NOTE_SAMPLE_DURATION = 0.7  # in seconds
    DEFAULT_SAMPLES_PER_NOTE = 3
    SILENCE_BEEP_FREQUENCY = 1200.0
    SILENCE_NOTE_NAME = "Stille"
    SILENCE_OCTAVE = ""

    def __init__(self,
                 sample_files_folder: str
                 = project_settings.SAMPLE_FILES_DIRECTORY_NAME,
                 csv_name: str = project_settings.CSV_NAME,
                 samples_per_note: int = DEFAULT_SAMPLES_PER_NOTE) -> None:
        self.__sample_files_folder = sample_files_folder
        self.__csv_name = csv_name
        self.__samples_per_note = samples_per_note
        self.__tuner = musicscale.Tuner()
        self.__recording_metadata = None
        self.__create_recording_metadata_with_user_input()
        self.__create_directory_for_this_recording()
        self.__save_csv_entry_for_this_recording()

    # noinspection PyMethodMayBeStatic
    def __generate_folder_name(self,
                               recording_metadata: Recording):
        return recording_metadata.instrument + "_" \
               + recording_metadata.timestamp

    def __create_directory_for_this_recording(self) -> None:
        folder_name = self.__generate_folder_name(self.__recording_metadata)
        path_name = os.path.join(self.__sample_files_folder, folder_name)
        os.mkdir(path_name)

    def __save_clip(self,
                    clip: audioclip.Audioclip,
                    sample_metadata: Sample) -> None:
        pitch = sample_metadata.note_name + str(sample_metadata.octave)
        file_name = (pitch + "_"
                     + str(sample_metadata.sample_index) + "_"
                     + sample_metadata.recording.timestamp + ".wav")
        file_path = os.path.join(self.__sample_files_folder,
                                 self.__generate_folder_name(
                                     sample_metadata.recording),
                                 file_name)
        clip.save(file_path)

    def __save_csv_entry_for_this_recording(self) -> None:
        row = [
            self.__recording_metadata.timestamp,
            self.__recording_metadata.instrument,
            self.__recording_metadata.instrument_type,
            self.__recording_metadata.extra_info
        ]
        with open(self.__csv_name, 'a') as file_handle:
            csv_writer = csv.writer(file_handle)
            csv_writer.writerow(row)

    def __record_note_dialog(self,
                             sample_metadata: Sample) -> audioclip.Audioclip:
        print("Zu spielende Note: {}{}, Aufnahme {}/{}".format(
            sample_metadata.note_name,
            sample_metadata.octave,
            sample_metadata.sample_index + 1,
            self.__samples_per_note))
        return self.__record_sound(sample_metadata.frequency)

    def __record_sound(self,
                       sound_sample_frequency: float) -> audioclip.Audioclip:
        while True:
            time.sleep(self.WAIT_TIME_BETWEEN_RECORDINGS)
            note_sample = audioclip.Audioclip.generate_sin_signal(
                sound_sample_frequency,
                self.NOTE_SAMPLE_DURATION)
            for _ in range(self.NUMBER_OF_REPETITIONS_IN_COUNTDOWN):
                note_sample.play()
                time.sleep(self.TIME_BETWEEN_REPETITIONS_IN_COUNTDOWN)
            # We need that delay to prevent note_sample to corrupt the recording
            time.sleep(self.BUFFER_TIME_AFTER_COUNTDOWN)
            print("Die Aufnahme läuft!")
            clip = audioclip.Audioclip.record()
            print("Folgendes wurde aufgenommen")
            clip.play()
            check: str = input("Ist die Aufnahme in Ordnung? (y/n) ")
            if check == "y":
                return clip

    # noinspection PyMethodMayBeStatic
    def __create_recording_metadata_with_user_input(self) -> None:
        instrument: str = input("Welches Instrument willst du Aufnehmen? ")
        instrument_type: str = input(
            "Welcher Instrumenttyp liegt vor (Hersteller, etc)? ")
        extra_info: str = input(
            "Hier kannst du zusätzliche Informationen eingeben :")
        timestamp = str(int(time.time()))
        self.__recording_metadata = \
            Recording(instrument, instrument_type, extra_info, timestamp)

    # noinspection PyMethodMayBeStatic
    def __wait_for_any_user_input_to_continue(self) -> None:
        input("Möchtest du beginnen? (jede Eingabe führt zum Start) ")

    # noinspection PyMethodMayBeStatic
    def __get_notes_we_want_to_record(self) -> List[Tuple[str, int, float]]:
        return self.__tuner.get_scale("c", 4, musicscale.Scales.ionisch)

    # noinspection PyMethodMayBeStatic
    def __generate_sample_metadata(self,
                                   note_of_sample: Tuple[str, int, float],
                                   sample_index: int) -> Sample:
        note_name, octave, frequency = note_of_sample
        return Sample(self.__recording_metadata, note_name, str(octave),
                      sample_index)

    # noinspection PyMethodMayBeStatic
    def __generate_silence_sample_metadata(self,
                                           sample_index: int) -> Sample:
        return Sample(self.__recording_metadata, self.SILENCE_NOTE_NAME,
                      self.SILENCE_OCTAVE, sample_index)

    def __record_silence_dialog(self,
                                sample_metadata: Sample) -> audioclip.Audioclip:
        print("Stille aufnhemen, Aufnahme {}/{}".format(
            sample_metadata.sample_index + 1,
            self.__samples_per_note))
        return self.__record_sound(sample_metadata.frequency)

    def __execute_note_recording_workflow_for_note(self,
                                                   note: Tuple[str, int, float]
                                                   ) -> None:
        for sample_index in range(self.__samples_per_note):
            sample_metadata = self.__generate_sample_metadata(note,
                                                              sample_index)
            clip = self.__record_note_dialog(sample_metadata)
            self.__save_clip(clip, sample_metadata)

    def __execute_silence_recording_workflow(self) -> None:
        for sample_index in range(self.__samples_per_note):
            sample_metadata = self.__generate_silence_sample_metadata(
                sample_index)
            clip = self.__record_silence_dialog(sample_metadata)
            self.__save_clip(clip, sample_metadata)

    def record_samples_dialog(self) -> None:
        """
        records all notes with certain amount of samples per note
        """
        self.__wait_for_any_user_input_to_continue()
        for note in self.__get_notes_we_want_to_record():
            self.__execute_note_recording_workflow_for_note(note)
        self.__execute_silence_recording_workflow()


if __name__ == "__main__":
    recorder = SoundRecorder()
    recorder.record_samples_dialog()
