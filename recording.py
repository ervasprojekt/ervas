class Recording:
    def __init__(self,
                 instrument: str,
                 instrument_type: str,
                 extra_info: str,
                 timestamp: str):
        self.__instrument = instrument
        self.__instrument_type = instrument_type
        self.__extra_info = extra_info
        self.__timestamp = timestamp

    @property
    def instrument(self) -> str:
        return self.__instrument

    @property
    def instrument_type(self) -> str:
        return self.__instrument_type

    @property
    def extra_info(self) -> str:
        return self.__extra_info

    @property
    def timestamp(self) -> str:
        return self.__timestamp

    def to_recording_metadata_dictionary(self):
        return {
            "instrument": self.__instrument,
            "instrument_type": self.__instrument_type,
            "extra_info": self.__extra_info,
            "timestamp": self.__timestamp
        }
