from typing import Tuple, List
import math
from enum import Enum


class Scales(Enum):
    ionisch = 0  # major/Dur
    dorisch = 1
    phrygisch = 2
    lydisch = 3
    mixolydisch = 4
    aeolisch = 5  # minor/Moll
    lokrisch = 6


class Tuner:
    """
    Convert notes to frequencies
    (and maybe also frequencies to notes, one day).

    Attributes:
        __NOTE_NAMES: Each GERMAN note name (b not equals h)
            is linked to a number so we can calculate a difference.
        __SCALE: List for assigning unique note names
        __SCALE_SPACING: The list defines the number of semitones that must
            be skipped to play a C major chord. This chord can be used to calculate all other scales.
    """
    __NOTE_NAMES = {
        "c": 0, "do": 0,
        "cis": 1, "des": 1,
        "d": 2, "re": 2,
        "dis": 3, "es": 3,
        "e": 4, "mi": 4,
        "f": 5, "fa": 5,
        "fis": 6, "ges": 6,
        "g": 7, "sol": 7,
        "gis": 8, "as": 8,
        "a": 9, "la": 9,
        "ais": 10, "b": 10,
        "h": 11, "si": 11,
    }
    __SCALE = ["c", "cis", "d", "dis", "e",
               "f", "fis", "g", "gis", "a",
               "ais", "h"]
    __SCALE_SPACING = [0, 2, 4, 5, 7, 9, 11]

    def __init__(self, reference_frequency: float = 440.0,
                 reference_note: str = "a",
                 reference_octave: int = 4) -> None:
        """
        Init Tuner with a reference note
        (defaults to Kammerton A defined by ISO 16.

        Args:
            reference_frequency: Frequency of the reverence note that is used
                for tuning all other notes (defaults to 440 Hz of Kammerton A4).
            reference_note:
                The name ("c", "cis", ...) of the reference note
                (defaults to "a" for Kammerton A4).
            reference_octave:
                The octave of the reference note
                (defaults to 4 for Kammerton A4).
        """
        if reference_note.lower() not in self.__NOTE_NAMES:
            raise KeyError("Unknown reference note: {}".format(reference_note))
        self.reference_frequency: float = reference_frequency
        self.reference_note: str = reference_note.lower()
        self.reference_octave: int = reference_octave

    def get_scale(
            self,
            keynote_name: str,
            octave: int,
            scale: Scales) -> List[Tuple[str, int, float]]:
        """
        Get all seven tones of a note scale of a certain key note.
        Args:
            keynote_name: The keynote name of the requested
                scale (e.g. a for A4).
            octave: Number of the octave (e.g. 4 for A4)
            scale: The scale (eg. Scales.ionisch for a major scale or
                Scales.aeolisch for a minor scale.)

        Returns:
            List of Tuples with note name, octave and
            frequency of the resulting note.
        """
        scale_number = scale.value
        basic_tone = Tuner.__SCALE_SPACING[scale_number]
        scale_notes = []
        for s in range(len(Tuner.__SCALE_SPACING)):
            new_tone = Tuner.__SCALE_SPACING[(scale_number + s) % len(
                Tuner.__SCALE_SPACING)]
            new_tone = (new_tone
                        - basic_tone
                        + math.floor((scale_number + s)
                                     / len(Tuner.__SCALE_SPACING))
                        * 12)
            scale_notes.append(
                self.__add_halftones_to_reference_notename(keynote_name,
                                                           octave, new_tone),
            )
        return scale_notes

    def get_note_frequency(
            self,
            note: str,
            octave: int = 4) -> float:
        """
        Calculates the frequency of a given note.

        Args:
            note: The name ("c", "cis", ...) of the note.
            octave: The octave of the reference note
                (defaults to 4 being in the same ocate as Kammerton A4).

        Returns:
            Frequency of the resulting note.
        """
        if note.lower() not in self.__NOTE_NAMES:
            raise KeyError("Unknown note: {}".format(note))
        note_number = (12 * octave
                       + self.__NOTE_NAMES[note.lower()])
        reference_note_number = (12 * self.reference_octave
                                 + self.__NOTE_NAMES[self.reference_note])
        note_diff_to_reference_note = note_number - reference_note_number
        frequency = (self.reference_frequency
                     * 2 ** (note_diff_to_reference_note / 12))
        frequency = Tuner.__add_halftones_to_reference_frequency(
            self.reference_frequency,
            note_diff_to_reference_note)
        return frequency

    @staticmethod
    def __add_halftones_to_reference_frequency(
            reference_frequency: float,
            number_of_halftones: int) -> float:
        """
        Use equal tuning to add halftones to a reference frequency.

        Args:
            reference_frequency: Frequency from which the tones are calculated.
            number_of_halftones: How many halftones shall be added
                (negative values are also possible).

        Returns:
            Frequency of the resulting note.
        """
        frequency = (reference_frequency
                     * 2 ** (number_of_halftones / 12))
        return frequency

    def __add_halftones_to_reference_notename(
            self,
            reference_note: str,
            reference_octave: int,
            number_of_halftones: int) -> Tuple[str, int, float]:
        """
        Determine the note name going up from a given note
        for a given number of halftones.

        Args:
            reference_note: Name of the reference note ("c", "cis", ...).
            reference_octave: The octave of the reference note
                (use 4 for the Kammerton A4).
            number_of_halftones: How many halftones shall be added
                (negative values are also possible).

        Returns:
            Tuple with note name, octave and frequency of the resulting note.
        """
        if reference_note.lower() not in Tuner.__NOTE_NAMES:
            raise KeyError("Unknown note: {}".format(reference_note))
        note_number = Tuner.__NOTE_NAMES[reference_note.lower()]
        note_number += number_of_halftones
        resulting_octave = reference_octave + math.floor(note_number / 12)
        note_number %= 12
        resulting_note = Tuner.__SCALE[note_number]
        resulting_frequency = self.get_note_frequency(resulting_note,
                                                      resulting_octave)
        return (resulting_note,
                resulting_octave,
                resulting_frequency)


if __name__ == "__main__":
    import audioclip
    import numpy as np

    standard_tuner = Tuner()
    scale = standard_tuner.get_scale("c", 4, Scales.aeolisch)
    melody = np.array([])
    for note in scale + scale[::-1]:
        freq = standard_tuner.get_note_frequency(*note)
        clip = audioclip.Audioclip.generate_sin_signal(freq, 0.1)
        melody = np.concatenate((melody, clip.audio))
    clip = audioclip.Audioclip(melody)
    clip.play()
