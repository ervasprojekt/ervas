import audioclip
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate


def resample_fourier_wrong(self, halftones: int):
    audio_data = self.audio
    data_ft = np.fft.rfft(audio_data)
    f = scipy.interpolate.interp1d(np.linspace(0, 1, len(data_ft)), data_ft, 'nearest', fill_value="extrapolate")
    resampled_data_ft = f(np.linspace(0, 2.0 ** (-halftones / 12), len(data_ft)))
    audio_data = np.fft.irfft(resampled_data_ft)
    return audioclip.Audioclip(audio_data, self.samplerate)

def resample(self, halftones: int):
    audio_data = self.audio
    f = scipy.interpolate.interp1d(np.linspace(0, 1, len(audio_data)), audio_data, 'nearest')
    sound_quotient = 2**(-halftones/12)
    resampled_data = f(np.linspace(0, 1, int(len(audio_data)*sound_quotient)))
    audio_data = resampled_data
    return audioclip.Audioclip(audio_data, self.samplerate)

def trim_silence(self):
    smooth_audio_data = self.smooth_audio_data()
    return audioclip.Audioclip(self.audio[smooth_audio_data > 0.005])

def smooth_audio_data(self):
    convolution_kernel = (np.ones((500,))
                          / 500)
    audio_data = np.convolve(np.abs(self.audio), convolution_kernel, 'same')
    return audio_data

audioclip.Audioclip.resample = resample
audioclip.Audioclip.trim_silence = trim_silence
audioclip.Audioclip.smooth_audio_data = smooth_audio_data

def play():
    soundclip = audioclip.Audioclip.load("./samples/Klavier_1559573099/a4_0_1559573099.wav")
    soundclip = soundclip.trim_silence()
    notes = {
        "c": 0, "do": 0,
        "cis": 1, "des": 1,
        "d": 2, "re": 2,
        "dis": 3, "es": 3,
        "e": 4, "mi": 4,
        "f": 5, "fa": 5,
        "fis": 6, "ges": 6,
        "g": 7, "sol": 7,
        "gis": 8, "as": 8,
        "a": 9, "la": 9,
        "ais": 10, "b": 10,
        "h": 11, "si": 11,
    }
    scaled_sounds = []
    for i in range(12):
        scaled_sounds.append(soundclip.resample(i))
    song = "cdefggaaaagaaaagffffeeddddc"
    for note in song:
        note_number = notes[note]
        scaled_sounds[note_number].play()

def resample_test():
    soundclip = audioclip.Audioclip.load("./samples/Klavier_1559573099/a4_0_1559573099.wav")
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex='col', sharey='col')
    time = np.linspace(0, soundclip.audio.size / soundclip.samplerate, soundclip.audio.size)
    ax1.plot(time, soundclip.audio)
    data_ft = np.fft.rfft(soundclip.audio)
    freq = np.fft.rfftfreq(soundclip.audio.size, d=1.0 / soundclip.samplerate)
    ax2.plot(freq, np.abs(data_ft))
    soundclip.play()
    soundclip = soundclip.resample(-2)
    soundclip.play()
    time = np.linspace(0, soundclip.audio.size / soundclip.samplerate, soundclip.audio.size)
    ax3.plot(time, soundclip.audio)
    data_ft = np.fft.rfft(soundclip.audio)
    freq = np.fft.rfftfreq(soundclip.audio.size, d=1.0 / soundclip.samplerate)
    ax4.plot(freq, np.abs(data_ft))
    plt.show()

resample_test()