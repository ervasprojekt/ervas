import numpy as np
from typing import Tuple, List, Callable
import sample_files_dataframe
import audioclip
import features

_DEPENDENT_VARIABLE = 'instrument'


def get_feature_array() -> Tuple[np.ndarray, np.ndarray]:
    """
    This mehtod uses the feature method list feature_calculating_functions
    specified in features.py to calculate a matrix of feature vectors X
    and a vector of the corresponding classes Y.

    Returns:
        A tuple consisting of the class vector Y and the feature matrix X
        as numpy ndarrays.
        A pandas dataframe would probably be prettier in future versions?
    """
    feature_calculating_functions: List[Callable[['audioclip.Audioclip'],
                                                 List[float]]]
    feature_calculating_functions = features.feature_calculating_functions
    classes = []
    feature_vectors = []
    sample_files = sample_files_dataframe.sample_files_dataframe()
    _preprocess_sample_dataframe(sample_files)
    for _, row in sample_files.iterrows():
        classifying_target = row[_DEPENDENT_VARIABLE]
        sample_path = row['path']
        classes.append(classifying_target)
        recording = audioclip.Audioclip.load(sample_path)
        feature_vectors.append(
            _calculate_features(recording, feature_calculating_functions))
    return (np.array(classes),
            np.array(feature_vectors))


def _preprocess_sample_dataframe(sample_files):
    sample_files.loc[sample_files["note"] == "Stille", "instrument"] = "Stille"


def _calculate_features(
        recording: audioclip.Audioclip,
        feature_calculating_functions: List[Callable[[audioclip.Audioclip],
                                                     List[float]]]
) -> List[float]:
    features_vector = []
    for feature_function in feature_calculating_functions:
        feature = feature_function(recording)
        features_vector+=feature
    return features_vector


if __name__ == '__main__':
    print(get_feature_array())
