import csv
import recording as recording_module
from typing import List, Tuple, Iterable
import os
import sample as sample_module
import project_settings


def create_list_of_sample_tuples_with_metadata_and_file_path() -> \
        List[Tuple[str, str, str, str, int, str, str, float, str]]:
    return __SampleListCreationWorkflow().create_list_of_sample_tuples_with_metadata_and_file_path()


class __SampleListCreationWorkflow:

    def __init__(self):
        self.__all_recordings = self.__create_list_of_recordings_from_csv_data()
        self.__current_recording = None
        self.__sample_directory = os.fsdecode(project_settings.SAMPLE_FILES_DIRECTORY_NAME)
        self.__current_file_path = self.__sample_directory
        self.__current_sample_folder_name = None
        self.__current_sample_file_name = None
        self.__current_sample = None

    # noinspection PyMethodMayBeStatic
    def __create_tuple_for_current_sample(self) -> Tuple[str, str, str, str, int, str, str, float, str]:
        return (
            self.__current_sample.recording.instrument,
            self.__current_sample.recording.instrument_type,
            self.__current_sample.recording.extra_info,
            self.__current_sample.recording.timestamp,
            self.__current_sample.sample_index,
            self.__current_sample.note_name,
            self.__current_sample.octave,
            self.__current_sample.frequency,
            self.__current_file_path
        )

    # Tuple order: instrument, instrument_type, extra_info, timestamp, sample_index, note_name, octave, freq, file_path
    def create_list_of_sample_tuples_with_metadata_and_file_path(self) -> \
            List[Tuple[str, str, str, str, int, str, str, float, str]]:
        result = []
        for folder in self.__create_sample_directory_iterator():
            self.__set_current_recording_folder_and_recording(folder)
            for file in self.__create_current_recording_folder_iterator():
                self.__set_current_sample_file_and_sample(file)
                result.append(self.__create_tuple_for_current_sample())
        return result

    def __create_sample_directory_iterator(self) -> Iterable[str]:
        return os.listdir(self.__sample_directory)

    def __create_current_recording_folder_iterator(self) -> Iterable[str]:
        return os.listdir(self.__current_file_path)

    def __set_current_recording_folder_and_recording(self, os_encoded_folder: str) -> None:
        self.__current_sample_folder_name = os.fsdecode(os_encoded_folder)
        self.__current_file_path = self.__get_file_path_for_current_sample_folder()
        self.__current_recording = self.__get_recording_for_current_sample_folder()
        self.__current_sample = None

    def __get_file_path_for_current_sample_folder(self) -> str:
        return os.path.join(self.__sample_directory, self.__current_sample_folder_name)

    def __set_current_sample_file_and_sample(self, os_encoded_file: str) -> None:
        self.__current_sample_file_name = os.fsdecode(os_encoded_file)
        self.__current_file_path = self.__get_file_path_for_current_sample_file()
        self.__current_sample = self.__create_sample_for_current_file()

    def __get_file_path_for_current_sample_file(self) -> str:
        return os.path.join(self.__get_file_path_for_current_sample_folder(), self.__current_sample_file_name)

    def __create_sample_for_current_file(self) -> sample_module.Sample:
        filename = self.__current_sample_file_name.split(".")[0]  # get rid of .wav
        note_name_and_octave, sample_index, timestamp = filename.split("_")
        note_name = note_name_and_octave.rstrip("0123456789")
        octave = note_name_and_octave[len(note_name):]
        return sample_module.Sample(self.__current_recording, note_name, octave, sample_index)

    # noinspection PyMethodMayBeStatic
    def __get_timestamp_for_current_folder(self) -> str:
        return self.__current_sample_folder_name.split("_")[1]

    def __get_recording_for_current_sample_folder(self) -> recording_module.Recording:
        # using the heuristic that timestamps are unique. While this doesn't have to be true, it is very likely
        for recording in self.__all_recordings:
            if recording.timestamp == self.__get_timestamp_for_current_folder():
                return recording
        raise KeyError

    def __create_list_of_recordings_from_csv_data(self) -> List[recording_module.Recording]:
        result = []
        for data_entry in self.__get_csv_data_from_file():
            if not data_entry:
                continue
            result.append(self.__create_recording_from_csv_data_entry(data_entry))
        return result

    # noinspection PyMethodMayBeStatic
    def __create_recording_from_csv_data_entry(self, data_entry: List) -> recording_module.Recording:
        return recording_module.Recording(
            data_entry[1],
            data_entry[2],
            data_entry[3],
            data_entry[0]
        )

    # noinspection PyMethodMayBeStatic
    def __get_csv_data_from_file(self) -> List[List]:
        with open(project_settings.CSV_NAME, "r", encoding="utf-8") as file:
            reader = csv.reader(file)
            result = list(reader)
            result.pop(0)  # pops the heading line
            return result


if __name__ == "__main__":
    for row in create_list_of_sample_tuples_with_metadata_and_file_path():
        print(row)
