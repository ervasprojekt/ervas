#!/usr/bin/env python3.6
# encoding: utf-8


"""Sound recording and analyzing tools."""

from typing import Optional, Tuple
import numpy as np
import sounddevice as sd
try:
    import readline
except ModuleNotFoundError:
    pass  #this is normal in windows
import soundfile as sf
import matplotlib.pyplot as plt

class Audioclip:
    """
    A single sound recording.
    """

    def __init__(self,
                 audio_data: np.ndarray,
                 samplerate: int = 44100,
                 ) -> None:
        """
        Inits audio samples and samplerate with supplied data.
        """
        self.__audio_data: np.ndarray = audio_data
        self.__samplerate: int = samplerate
        self.filename = ""

    @staticmethod
    def record(duration: int = 5,
               samplerate: int = 44100,
               channels: int = 1) -> 'Audioclip':
        """Factory method to record audio.

        Args:
            duration: The duration of the audio in seconds.
            samplerate: The samplerate of the audio in Hz.
            channels: Number of channels to record (1=mono, 2=stereo).

        Returns:
            The recorded Audioclip.
        """
        number_of_samples = int(duration * samplerate)
        audio_data = sd.rec(number_of_samples,
                            samplerate=samplerate,
                            channels=channels,
                            blocking=True)
        return Audioclip(audio_data, samplerate)

    def play(self, blocking: bool = True) -> None:
        """Plays the recorded audio.

        Args:
            blocking: Indicates whether the method shall wait for the playback
                to finish. Defaults to True.
        """
        sd.play(self.__audio_data, self.__samplerate, blocking=blocking)

    def save(self, filename: str) -> None:
        """Save the recorded audio to a file.

        Args:
            filename: Indicates where the audio shall be saved.
        """
        sf.write(filename, self.__audio_data, self.__samplerate)

    @staticmethod
    def load(filename: str) -> 'Audioclip':
        """Factory method to load audio from a file.

        Args:
            filename: File with the recoding
                (see soundfile documentation for supported formats).
        """
        audio_data, samplerate = sf.read(filename)
        new_audio = Audioclip(audio_data, samplerate)
        new_audio.filename = filename
        return new_audio

    @property
    def audio(self) -> np.ndarray:
        """Get the audio of the recording.

        Returns:
            A reference to the audio signal data.
        """
        return self.__audio_data

    @property
    def samplerate(self) -> int:
        """
        Get the samplerate of the recording.

        Returns:
            Samplerate of the recording.
        """
        return self.__samplerate

    @staticmethod
    def generate_sin_signal(frequency: float,
                            duration: float,
                            samplerate: int = 44100) -> 'Audioclip':
        """
        Generates a sinus signal with given frequency, duration and samplerate.

        Args:
            frequency: Frequency of the generated signal.
            duration: The duration of the audio in seconds.
            samplerate: The samplerate of the audio in Hz.

        Returns:
           Audioclip of the generated signal.
        """
        wave_x = np.linspace(0, 2 * np.pi * frequency * duration, samplerate * duration)
        wave = np.sin(wave_x)
        return Audioclip(wave, samplerate)

    def normalize(self) -> None:
        """
        Normalizes the audio_data so that it ranges from -1 to 1.
        """
        max_value = np.max(self.__audio_data)
        min_value = np.min(self.__audio_data)
        if max_value != min_value:
            self.__audio_data -= min_value
            self.__audio_data /= 0.5 * (max_value - min_value)
            self.__audio_data -= 1.0



