from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
import joblib
import sample_file_features


random_state = 1
y, x = sample_file_features.get_feature_array()
classifier = SVC(probability=True)
"""RandomForestClassifier(criterion='entropy',
                                    max_depth=5,
                                    n_estimators=100,
                                    max_features='auto',
                                    random_state=random_state)
"""

scaler = StandardScaler()
x = scaler.fit_transform(x)
classifier.fit(x, y)
score_train = classifier.score(x, y)
print(classifier.score(x, y))
joblib.dump(scaler, 'scaler.pkl')
joblib.dump(classifier, 'classifier.pkl')
