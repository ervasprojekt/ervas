import recording as recording_module
import musicscale


class Sample:
    def __init__(self,
                 recording_instance: recording_module.Recording,
                 note_name: str,
                 octave: str,
                 sample_index: int):
        self.__recording = recording_instance
        self.__note_name = note_name
        self.__octave = octave
        self.__sample_index = sample_index
        if note_name == "Stille":
            self.__frequency = 1200.0
        else:
            self.__frequency = musicscale.Tuner().get_note_frequency(note_name, int(octave))

    @property
    def recording(self) -> recording_module.Recording:
        return self.__recording

    @property
    def note_name(self) -> str:
        return self.__note_name

    @property
    def octave(self) -> str:
        return self.__octave

    @property
    def sample_index(self) -> int:
        return self.__sample_index

    @property
    def frequency(self) -> float:
        return self.__frequency

    def to_sample_metadata_dictionary(self) -> dict:
        return {
            "note_name": self.__note_name,
            "octave": self.__octave,
            "frequency": self.__frequency,
            "sample_index": self.__sample_index,
            "recording_metadata": self.__recording.to_recording_metadata_dictionary()
        }


if __name__ == "__main__":
    print("module sample is not supposed to be called as main")
