from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import sample_file_features
from sklearn.preprocessing import StandardScaler

y, x = sample_file_features.get_feature_array()
x = StandardScaler().fit_transform(x)

pca = PCA(n_components=2, random_state=1)
pca.fit(x)
x = pca.transform(x)

plt.figure(figsize=(12, 8))
ax = plt.subplot(111)

for instrument in set(y):
    x_with_this_instrument = x[y == instrument]
    ax.scatter(x_with_this_instrument[:, 0], x_with_this_instrument[:, 1],
               label=instrument)
ax.legend()
plt.show()
