import numpy as np
from typing import List, Callable
import audioclip
import numpy as  np
import scipy.signal
import librosa


"""
This module specifies a list of functions that are used for calculating the
feature vectors in the variable feature_calculating_functions.
"""

WINDOW_SIZE_IN_SECONDS = 3
SOUND_THRESHOLD = 0.005  # Anything below this will be detected as silence.
CONVOLUTION_KERNEL_SIZE = 500


def sound_preprocessing(recording: audioclip.Audioclip):
    audio_data = _trim_leading_silence(recording)
    audio_data = audio_data[:recording.samplerate * WINDOW_SIZE_IN_SECONDS]
    return audioclip.Audioclip(audio_data, recording.samplerate)


def _trim_leading_silence(recording: audioclip.Audioclip):
    audio_data = recording.audio
    smooth_audio_data = _smooth_audio_data(audio_data)
    sound_start = np.argmax(smooth_audio_data > SOUND_THRESHOLD)
    if (sound_start < recording.samplerate * 1):
        return audio_data[sound_start:]
    return audio_data


def _smooth_audio_data(audio_data: np.ndarray):
    convolution_kernel = (np.ones((CONVOLUTION_KERNEL_SIZE,))
                          / CONVOLUTION_KERNEL_SIZE)
    audio_data = np.convolve(np.abs(audio_data), convolution_kernel, 'same')
    return audio_data


def fourier(recording: audioclip.Audioclip) -> List[float]:
    audio_data = recording.audio
    data_ft = np.fft.rfft(audio_data)
    # This is how the frequencies could be calculated:
    # freq = np.fft.rfftfreq(audio_data.size, d=1.0 / recording.samplerate)
    return np.absolute(data_ft).tolist()


def main_frequencies(recording: audioclip.Audioclip) -> List[float]:
    audio_data = recording.audio
    data_ft = np.fft.rfft(audio_data)
    data_ft = data_ft / data_ft.size
    freq = np.fft.rfftfreq(audio_data.size, d=1.0 / recording.samplerate)
    maxima_indices = scipy.signal.argrelextrema(data_ft, np.greater, order=50)
    maxima_indices = maxima_indices[0]
    maxima_values = np.abs(data_ft[maxima_indices])
    maxima_indices = maxima_indices[np.argsort(maxima_values)]
    maxima_indices = maxima_indices[::-1]
    freq = freq[maxima_indices]
    freq = freq[:3]
    #print(recording.filename)
    #print(freq)
    return freq.tolist()


def _calculate_overtones(main_freq, overtones=64, pivot=0) -> List[float]:
    frequencies = []

    for i in range(overtones + 1):
        n = int((pivot / 2 + 0.5) * overtones) - (overtones) + i
        if n < 0:
            n = 1 / abs(n)
        if n != 0:
            frequencies.append(main_freq * n)
        if n == 1.0:
            main_freq_index = i
    return frequencies, main_freq_index


def overtones(recording: audioclip.Audioclip) -> List[float]:
    audio_data = recording.audio
    data_ft = np.fft.rfft(audio_data)
    freq = np.absolute(np.fft.rfftfreq(audio_data.size, d=1.0 / recording.samplerate)).tolist()
    mag = np.absolute(data_ft).tolist()
    # get main frequency
    main_freq_index = mag.index(max(mag))
    main_freq = freq[main_freq_index]
    overtones, main_freq_index = _calculate_overtones(main_freq, 64, 0.6)

    # find magnitudes for overtone-frequencies
    overtone_mag = []
    for overtone in overtones:
        found_freq = False
        for i in range(len(freq) - 1):
            if freq[i] <= overtone <= freq[i + 1]:
                # use mean average for magnitude measurement
                try:
                    overtone_mag.append(
                        (mag[i] + mag[i - 1] + mag[i + 1] + mag[i - 2] + mag[i + 2]) / 5)
                except:
                    print("Warning: Overtone frequency mean calculation: index out of Range, using single frequency")
                    overtone_mag.append(mag[i])
                found_freq = True
                break
        if not found_freq:
            # if overtone-frequency is out of range
            overtone_mag.append(0)
    # normalise to main frequency magnitude
    main_freq_mag = overtone_mag[main_freq_index]
    for i in range(len(overtone_mag)):
        overtone_mag[i] = overtone_mag[i] / main_freq_mag
    return overtone_mag


def fourier_freq_adjusted(recording: audioclip.Audioclip) -> List[float]:
    audio_data = recording.audio
    data_ft = np.fft.rfft(audio_data)
    mag = np.absolute(data_ft).tolist()
    # get main frequency index
    main_freq_index = mag.index(max(mag))
    mag_adjusted = mag[main_freq_index: main_freq_index + 19000]

    return mag_adjusted


def amplitude(recording: audioclip.Audioclip) -> List[float]:
    NUMBER_OF_WINDOWS = 5
    #recording = sound_preprocessing(recording)
    windows = np.split(recording.audio, NUMBER_OF_WINDOWS)
    window_mean = []
    for w in windows:
        window_mean.append(float(np.mean(np.absolute(w))))
    return window_mean

def mfcc(recording: audioclip.Audioclip) -> List[float]:
    mfccs = librosa.feature.mfcc(recording.audio, recording.samplerate, n_mfcc=5)
    return mfccs.flatten().tolist()

def melspectrogram(recording: audioclip.Audioclip) -> List[float]:
    spect = librosa.feature.melspectrogram(recording.audio, recording.samplerate)
    return spect.flatten().tolist()

feature_calculating_functions: List[
    Callable[[audioclip.Audioclip], List[float]]
] = [amplitude, mfcc]
