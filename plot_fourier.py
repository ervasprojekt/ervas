import audioclip
import tkinter
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import sys


def _plot_audioclip_img(sample):
    fig, (ax1, ax2) = plt.subplots(2, 1)
    x_values = np.linspace(
        0,
        sample.audio.size / sample.samplerate,
        sample.audio.size)
    ax1.plot(x_values, sample.audio)

    data_ft = np.fft.fft(sample.audio)
    freq = np.fft.fftfreq(sample.audio.size, d=1.0 / sample.samplerate)
    data_ft = np.abs(data_ft)
    ax2.plot(freq, data_ft / data_ft.size)
    plt.show()


def _plot_audioclip(sample):
    fig, (ax1, ax2) = plt.subplots(2, 1)
    x_values = np.linspace(
        0,
        sample.audio.size / sample.samplerate,
        sample.audio.size)
    ax1.plot(x_values, sample.audio)

    data_ft = np.fft.rfft(sample.audio)
    freq = np.fft.rfftfreq(sample.audio.size, d=1.0 / sample.samplerate)
    data_ft = np.abs(data_ft)
    ax2.plot(freq, data_ft / data_ft.size)
    plt.show()


def _fade(sample):
    ampli = np.linspace(-1, 1, len(sample.audio))
    ampli = 1 - ampli ** 2
    faded_audio = audioclip.Audioclip(sample.audio * ampli, sample.samplerate)
    return faded_audio


def _fade2(sample):
    ampli = np.linspace(-1, 1, len(sample.audio))
    ampli = np.ones(len(sample.audio)) * np.abs(ampli) < 0.5
    faded_audio = audioclip.Audioclip(sample.audio * ampli, sample.samplerate)
    return faded_audio


def example1():
    """
    Ein einfaches Sinus-Signal mit 440 Hz
    """
    sample = audioclip.Audioclip.generate_sin_signal(440, 1)
    sample.play()
    _plot_audioclip_img(sample)


def example2():
    """
    Ein einfaches Sinus-Signal mit 440 Hz (Realwertige Fouriertransformation)
    """
    sample = audioclip.Audioclip.generate_sin_signal(440, 1)
    sample.play()
    _plot_audioclip(sample)


def example3():
    """
    Zwei ueberlagerte Sinus-Signale mit 440 und 880 Hz
    """
    sample1 = audioclip.Audioclip.generate_sin_signal(440, 1)
    sample2 = audioclip.Audioclip.generate_sin_signal(880, 1)
    sample = audioclip.Audioclip(sample1.audio + sample2.audio)
    sample.play()
    _plot_audioclip(sample)


def example4():
    """
    Saegezahnsignal
    """
    zeit = np.linspace(0, 2 * np.pi, 44100)
    audio_data = signal.sawtooth(440 * zeit)
    sample = audioclip.Audioclip(audio_data)
    sample.play()
    _plot_audioclip(sample)


def example5():
    """
    Ein einfaches Sinus-Signal mit 440 Hz, ein- und ausgeblendet
    """
    sample = audioclip.Audioclip.generate_sin_signal(440, 1)
    sample = _fade(sample)
    sample.play()
    _plot_audioclip(sample)


def example6():
    """
    Ein einfaches Sinus-Signal mit 440 Hz, ein- und ausgeblendet
    """
    sample = audioclip.Audioclip.generate_sin_signal(440, 1)
    sample = _fade2(sample)
    sample.play()
    _plot_audioclip(sample)


def example7():
    """
    Sound-Datei laden
    """
    root_window = tkinter.Tk()
    root_window.withdraw()
    audio_filename = tkinter.filedialog.askopenfilename(
        defaultextension="wav",
        title="Welche Audio-Datei soll geladen werden?",
        filetypes=(('Aduio-Datei', '.wav'),))
    sample = audioclip.Audioclip.load(audio_filename)
    sample.play()
    _plot_audioclip(sample)


example1()
