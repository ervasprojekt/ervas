import sounddevice as sd
import numpy as np
import queue
import audioclip
import features
import joblib
import sys

SAMPLERATE = 44100
WINDOW_SIZE_IN_SECONDS = 5


def audio_callback(indata, frames, time, status):
    if status:
        print(status, file=sys.stderr)
    q.put(indata[:, 0])


def calculate_features(data):
    clip = audioclip.Audioclip(data, SAMPLERATE)
    feature_calculating_functions = features.feature_calculating_functions
    features_vector = []
    for feature_function in feature_calculating_functions:
        feature = feature_function(clip)
        features_vector += feature
    return features_vector


q = queue.Queue()
data = np.zeros(SAMPLERATE * WINDOW_SIZE_IN_SECONDS)
stream = sd.InputStream(channels=1, samplerate=SAMPLERATE,
                        callback=audio_callback)
classifier = joblib.load('classifier.pkl')
scaler = joblib.load('scaler.pkl')

with stream:
    while True:
        new_data = None
        try:
            while True:
                new_data = q.get_nowait()
                new_data_size = len(new_data)
                data = np.roll(data, -new_data_size)
                data[-new_data_size:] = new_data
        except:
            pass
        feature_vector = calculate_features(data)
        feature_vector = np.array(feature_vector).reshape(1, -1)
        feature_vector = scaler.transform(feature_vector)
        class_probs = zip(classifier.classes_,
                          classifier.predict_proba(feature_vector)[0])
        print(max(class_probs, key=lambda x: x[1]))
