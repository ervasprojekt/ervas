import pandas as pd
import project_settings
import pathlib
from typing import Dict, Any


_SAMPLE_FILE_PATTERN = '**/*.wav'
_CSV_NAME = project_settings.CSV_NAME
_SAMPLE_FILES_DIRECTORY_NAME = project_settings.SAMPLE_FILES_DIRECTORY_NAME
_PATH_COLUMN_NAME = 'path'
_JOIN_TABLES_ON_COLUMN = 'timestamp'
_FILENAME_INFOS = [
    'note',
    'sample',
    'timestamp',
]
_FILENAME_DELIMITER = "_"


def sample_files_dataframe() -> pd.DataFrame:
    meta_data = pd.read_csv(_CSV_NAME, skipinitialspace=True)
    sample_paths_and_timestamps = _get_paths_of_sample_files()
    sample_files = pd.merge(meta_data,
                            sample_paths_and_timestamps,
                            on=_JOIN_TABLES_ON_COLUMN)
    return sample_files


def _get_paths_of_sample_files() -> pd.DataFrame:
    column_names = [_PATH_COLUMN_NAME]+_FILENAME_INFOS
    file_table = pd.DataFrame(columns=column_names)
    sample_files_path = pathlib.Path(_SAMPLE_FILES_DIRECTORY_NAME)
    sample_files = sample_files_path.glob(_SAMPLE_FILE_PATTERN)
    for sample_file in sample_files:
        file_infos = _get_infos_from_filename(sample_file)
        file_table = file_table.append(file_infos,
                                       ignore_index=True)
    return file_table


def _get_infos_from_filename(sample_file) -> Dict[str, Any]:
    file_infos = {}
    file_infos[_PATH_COLUMN_NAME] = str(sample_file)
    file_stem = sample_file.stem
    split_filename = file_stem.split(_FILENAME_DELIMITER)
    for i, column_name in enumerate(_FILENAME_INFOS):
        file_infos[column_name] = split_filename[i]
        if file_infos[column_name].isdigit():
            file_infos[column_name] = int(file_infos[column_name])
    return file_infos


if __name__ == '__main__':
    print(sample_files_dataframe().head)
